# MIAGE MPROJ

## Plan du cours

### Séance 1: Découverte de l'entreprise
- Présentation du cours
#### Introduction
- Faire connaissance
- Qu'attendez-vous de ce cours
#### Cours: Découverte de l'entreprise
- Rappel sur la gestion de projets
- Video sur l'entreprise liberé
- Definition d'une entreprise
- Faire un organigramme
- Definir le management de projet
- Lecture du rapport Chaos
#### TP
- Constitution des équipes
- Mon projet
- La céréal Box

### Séance 2: Conceptualiser un projet
#### Introduction
- Entreprendre aujourd'hui
- Video d'Emmanuelle Duez
- Retour sur la ceral box
- La note du public
#### Cours: Lean Canvas & pitcher un projet
- Notion de concept
- L'elevator pitch
- Pitcher son projet
- Video sur le lean canvas
- Le lean canvas partie 1
#### TP
- Réaliser le lean canvas de son projet


### Séance 3: Modéliser un projet
#### Introduction
- Savoir se présenter en entreprise
#### Cours: Découpage, epic et User story
- MVP: Minimum Viable Product
- Lean Start-up
- Les epics
- Les user stories
- Les features
- Split poker: découpage de user stories
#### TP
- Réaliser le MVP de son projet et le découpage en US et features

### Séance 4: Chiffrage 
#### Introduction
- Onboarding d'un membre
- La note du public
- Retour sur le lean canvas
#### Cours: Estimer et chiffrer son projet
- Chiffrage vs estimation
- Points de conplexité
- Poker Planning
- Extreme Quotation
- No estimate
- La confiance d'un chiffrage
- L'Abbaque
#### TP
- Réaliser l'abbaque de chiffrage

### Séance 5: Priorisation
#### Introduction
- Pas de sujet d'intro (temps sur le tp de chiffrage)
- La note du public sur MVP et découpage
- Retour sur le Découpage
#### Cours: Prioriser
- Perdu dans le desert
#### TP
- Pas de tp


### Séance 6: Roadmap
#### Introduction
- La note du public sur le chiffrage
- Retour sur le chiffrage
#### Cours: Roadmap & Priorités
- exo: l'ile desert
- les outils de priorisation
- exo: perdu sur mars
- le role des roadmaps
- Le Go product roadmap
- le lean roadmap
#### TP
- Effectuer la roadmap du projet

### Séance 7: Kick off
#### Introduction
- negocier son salaire
- La note du public sur la roadmap
- Retour sur la roadmap
#### Cours: Kick off

#### TP

### Séance 8: Animation d'atelier & Kick Off 1/2
#### Introduction
- Le management 3.0


### Séance 9: Animation d'atelier & brainstorm 2/2

### Séance 10: Retrospective du cours


## les outils

- brainstorm
- DOD / DOR
- MOSCOW

## sujet d'intro
- participation au communauté (conf ...)
- montage financier d'une entreprise
- propriété du code
- importance du rsx
- plan du MBA  -> la valeur + le marketing +la vente+ la distribution + le financement
- Communication l'image que l'on souhaite donner


## a classer
rapport Chaos -> agilité

## Bibliothèque
- Game Storming
- Lean Start up
- Scrum guide




<p align="center">
  <a href="https://revealjs.com">
  <img src="https://hakim-static.s3.amazonaws.com/reveal-js/logo/v1/reveal-black-text-sticker.png" alt="reveal.js" width="500">
  </a>
  <br><br>
  <a href="https://github.com/hakimel/reveal.js/actions"><img src="https://github.com/hakimel/reveal.js/workflows/tests/badge.svg"></a>
  <a href="https://slides.com/"><img src="https://s3.amazonaws.com/static.slid.es/images/slides-github-banner-320x40.png?1" alt="Slides" width="160" height="20"></a>
</p>

reveal.js is an open source HTML presentation framework. It enables anyone with a web browser to create beautiful presentations for free. Check out the live demo at [revealjs.com](https://revealjs.com/).

The framework comes with a powerful feature set including [nested slides](https://revealjs.com/vertical-slides/), [Markdown support](https://revealjs.com/markdown/), [Auto-Animate](https://revealjs.com/auto-animate/), [PDF export](https://revealjs.com/pdf-export/), [speaker notes](https://revealjs.com/speaker-view/), [LaTeX typesetting](https://revealjs.com/math/), [syntax highlighted code](https://revealjs.com/code/) and an [extensive API](https://revealjs.com/api/).

---

Want to create reveal.js presentation in a graphical editor? Try <https://slides.com>. It's made by the same people behind reveal.js.

---

### Sponsors
Hakim's open source work is supported by <a href="https://github.com/sponsors/hakimel">GitHub sponsors</a>. Special thanks to:
<div align="center">
  <table>
    <td align="center">
      <a href="https://workos.com/?utm_campaign=github_repo&utm_medium=referral&utm_content=revealjs&utm_source=github">
        <div>
          <img src="https://user-images.githubusercontent.com/629429/151508669-efb4c3b3-8fe3-45eb-8e47-e9510b5f0af1.svg" width="290" alt="WorkOS">
        </div>
        <b>Your app, enterprise-ready.</b>
        <div>
          <sub>Start selling to enterprise customers with just a few lines of code. Add Single Sign-On (and more) in minutes instead of months.</sup>
        </div>
      </a>
    </td>
    <td align="center">
      <a href="https://www.doppler.com/?utm_campaign=github_repo&utm_medium=referral&utm_content=revealjs&utm_source=github">
        <div>
          <img src="https://user-images.githubusercontent.com/629429/151510865-9fd454f1-fd8c-4df4-b227-a54b87313db4.png" width="290" alt="Doppler">
        </div>
        <b>All your environment variables, in one place</b>
        <div>
          <sub>Stop struggling with scattered API keys, hacking together home-brewed tools, and avoiding access controls. Keep your team and servers in sync with Doppler.</sup>
        </div>
      </a>
    </td>
  </table>
</div>

---

### Getting started
- 🚀 [Install reveal.js](https://revealjs.com/installation)
- 👀 [View the demo presentation](https://revealjs.com/demo)
- 📖 [Read the documentation](https://revealjs.com/markup/)
- 🖌 [Try the visual editor for reveal.js at Slides.com](https://slides.com/)
- 🎬 [Watch the reveal.js video course (paid)](https://revealjs.com/course)

--- 
<div align="center">
  MIT licensed | Copyright © 2011-2022 Hakim El Hattab, https://hakim.se
</div>
